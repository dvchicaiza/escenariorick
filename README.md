# escenarioRick

Este programa despliega el escenario de Rick en 3d, haciendo uso de la librería OpenGL y sus librerías relacionadas. Implementa cinco cámaras con diferentes puntos de vista tanto estáticos como dinámicos.

Mi proyecto contiene la carpeta ProyectoEscenario que contiene los archivos correspondientes al programa. Dentro estan subcarpetas de las cuales en la ruta src/org/dchicaiza se encuentran las clases .java descritas:

1. Casa.java
2. DetallePatio.java
3. ExtGarage.java
4. IntGarage.java
5. Nave.java
6. ProyectoEscenario.java

La clase ProyectoEscenario es la clase principal y las otras clases contienen el codigo necesario para dibujar cada uno de los objetos que constituyen el escenario.

A continuacion se muestra una imagen de la ejecución del proyecto, con el punto de vista de la cámara frontal.
![mi escenario 3d](https://drive.google.com/file/d/1P4UvT0yrTRJKmA5tfT7z7joRmci7YKoL/view?usp=sharing)

