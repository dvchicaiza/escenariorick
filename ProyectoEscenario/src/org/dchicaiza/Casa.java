/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dchicaiza;
import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
/**
 *
 * @author Doris
 */
public class Casa {
    GL gl;
    GLUT glut;
    
    public Casa(GL gl, GLUT glut){
        this.gl=gl; 
        this.glut=glut;
    }
    
        void dibujaCubo(float PX, float PY, float PZ, float L, float r, float g, float b){
            gl.glPushMatrix();
            gl.glTranslated(PX,PY,PZ);
            gl.glPushMatrix();
            gl.glPopMatrix();
            gl.glColor3f(r, g, b);
            glut.glutSolidCube(L);
            gl.glPopMatrix();
        }
        
        public void dibujaCilindro(double R, double A, int sl, int st, float r, float g, float b, float angle, float PX, float PY, float PZ) {
            gl.glPushMatrix();
            gl.glTranslatef(PX,PY,PZ);
            gl.glRotated(angle, 1, 0, 0);
            gl.glColor3f(r,g,b);
            glut.glutSolidCylinder(R,A,sl,st);
            gl.glPopMatrix();
        }
        
        public void dibujaCuadrado(float x1, float y1,float z1, float x2, float y2, float z2, float x3, float y3, float z3, float x4, float y4,float z4, float r, float g, float b,float t){
            gl.glPushMatrix();
            gl.glColor4f(r, g, b,t);
            gl.glBegin(GL.GL_QUADS);
            gl.glVertex3f(x1, y1, z1);
            gl.glVertex3f(x2, y2, z2);
            gl.glVertex3f(x3, y3, z3);
            gl.glVertex3f(x4, y4, z4);
            gl.glEnd();
            gl.glPopMatrix();
        }
        
        void dibujaTriangulo(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3,float z3, float r, float g, float b){
            gl.glPushMatrix();
            gl.glColor3f(r,g,b);
            gl.glBegin(GL.GL_TRIANGLES);
            gl.glVertex3f(x1, y1, z1);
            gl.glVertex3f(x2, y2, z2);
            gl.glVertex3f(x3, y3, z3);
            gl.glEnd();
            gl.glPopMatrix();
        }
        
        void dibujaLinea(float x1, float y1,float z1, float x2, float y2, float z2, float x3, float y3, float z3, float x4, float y4,float z4, float r, float g, float b){
            gl.glPushMatrix();
            gl.glColor3f(r, g, b);
            gl.glBegin(GL.GL_LINE_LOOP);
            gl.glVertex3f(x1, y1, z1);
            gl.glVertex3f(x2, y2, z2);
            gl.glVertex3f(x3, y3, z3);
            gl.glVertex3f(x4, y4, z4);
            gl.glEnd();
            gl.glPopMatrix();
        }
        
        public void dibujarCasa(){
            //BLOQUE DERECHO POSTERIOR
            dibujaCuadrado(18, 0.01f, 10, 28, 0.01f, 10, 28, 0.01f, -28, 18, 0.01f, -28, 0.35f, 0.0f, 0.0f,1);//piso primer bloque
            dibujaCuadrado(28, 0, -28, 28, 0, 10, 28, 20, 10, 28, 20, -28, 1, 1, 0.6f,1);//pared costado derecho
            dibujaCuadrado(18, 0, -28, 28, 0, -28, 28, 20, -28, 18, 20, -28, 1, 1, 0.6f,1);//pared fondo
            dibujaTriangulo(18, 20, -28, 28, 20, -28, 23, 25, -18, 0.7f, 0.3f, 0.1f);//techo fondo 
            dibujaCuadrado(28, 20, -28, 28, 20, 10, 23, 25, 0, 23, 25, -18, 0.7f, 0.3f, 0.1f,1);//techo parte derecha
            dibujaCuadrado(18, 20, -28, 18, 20, 10, 23, 25, 0, 23, 25, -18, 0.7f, 0.3f, 0.1f,1);//techo parte izquierda
            dibujaCuadrado(18, 0, -28, 18, 0, 10, 18, 20, 10, 18, 20, -28, 1, 1, 0.6f,1);//pared izquierda
            dibujaTriangulo(18, 20, 10, 28, 20, 10, 23, 25, 0, 0.7f, 0.3f, 0.1f);//techo frente
            dibujaCuadrado(18, 0, 10, 28, 0, 10, 28, 20, 10, 18, 20, 10, 1, 1, 0.6f,1);//pared frente
            
            //ventana superior primer bloque
            gl.glPushMatrix();
            gl.glTranslatef(0, 10f, -10f);
            dibujaCuadrado(20, 3.5f, 20.001f, 26, 3.5f, 20.001f, 26, 9, 20.001f, 20, 9, 20.001f, 0.9f, 0.4f, 0.2f,1);//base ventana
            dibujaCuadrado(20.3f, 3.8f, 20.01f, 22.9f, 3.8f, 20.01f, 22.9f, 8.7f, 20.01f, 20.3f, 8.7f, 20.01f, 0.0f, 0.5f, 1.0f,1);//cristal izquierdo
            dibujaCuadrado(23.1f, 3.8f, 20.01f, 25.7f, 3.8f, 20.01f, 25.7f, 8.7f, 20.01f, 23.1f, 8.7f, 20.01f, 0.0f, 0.5f, 1.0f,1);//cristal derecho
            gl.glPopMatrix();
            
            //balc�n con cilindro GLUT 
            //rejas izquierdas
            dibujaCilindro(0.3f, 2.5f, 9, 9, 0.9f, 0.4f, 0.2f, 90, 18.35f, 14.5f, 12.8f);//reja fondo
            dibujaCilindro(0.3f, 2.5f, 9, 9, 0.9f, 0.4f, 0.2f, 90, 18.35f, 14.5f, 15.8f);//reja medio
            //rejas derechas
            dibujaCilindro(0.3f, 2.5f, 9, 9, 0.9f, 0.4f, 0.2f, 90, 27.65f, 14.5f, 12.8f);//reja fondo
            dibujaCilindro(0.3f, 2.5f, 9, 9, 0.9f, 0.4f, 0.2f, 90, 27.65f, 14.5f, 15.8f);//reja medio
            
            //rejas frontales de izquierda a derecha
            gl.glPushMatrix();
            for (int i = 0; i < 7; i++) {               
                dibujaCilindro(0.3f, 2.5f, 9, 9, 0.9f, 0.4f, 0.2f, 90, 18.35f, 14.5f, 18.7f);
                gl.glTranslatef(1.55f, 0, 0);
            }
            gl.glPopMatrix();
            
            //rejas superiores
            gl.glPushMatrix();
            gl.glRotated(90, 0, 1, 0);
            gl.glColor3f(0.9f, 0.4f, 0.2f);
            gl.glTranslatef(-18.75f, 14.78f, 18.1f);
            glut.glutSolidCylinder(.3f, 9.8f, 9, 9);//reja frente           
            gl.glPopMatrix();
            dibujaCilindro(0.3f, 8.6f, 9, 9, 0.9f, 0.4f, 0.2f, 0, 18.35f, 14.78f, 10);// reja izquierda
            dibujaCilindro(0.3f, 8.6f, 9, 9, 0.9f, 0.4f, 0.2f, 0, 27.65f, 14.78f, 10);//reja derecha
            
            //BLOQUE FRONTAL DERECHO
            dibujaCubo(23, 5, 15, 10, 1, 1, 0.6f); //figura Glut utilizada para bloque frontal derecho
            dibujaCuadrado(18, 12, 10, 18, 12, 19, 28, 12, 19, 28, 12, 10, 0.7f, 0.3f, 0.1f,1);//techo parte superior
            dibujaCuadrado(17, 10, 9, 17, 10, 20, 18, 12, 19, 18, 12, 10, 0.7f, 0.3f, 0.1f,1);//techo parte izquierda
            dibujaCuadrado(29, 10, 20, 29, 10, 9, 28, 12, 10, 28, 12, 19, 0.7f, 0.3f, 0.1f,1);//techo pate derecha
            dibujaCuadrado(17, 10, 20, 29, 10, 20, 28, 12, 19, 18, 12, 19, 0.7f, 0.3f, 0.1f,1);//techo parte frontal 
            dibujaCuadrado(17, 10, 9, 17, 10, 20, 29, 10, 20, 29, 10, 9, 0.7f, 0.3f, 0.1f,1);//techo parte inferior
            dibujaTriangulo(17, 10, 9, 18, 10, 9, 18, 12, 10, 0.7f, 0.3f, 0.1f);//cierra las salidas del techo izquierdo
            dibujaTriangulo(28, 10, 9, 29, 10, 9, 28, 12, 10, 0.7f, 0.3f, 0.1f);//cierra las salidas del techo derecho
            
            //ventana bloque frontal
            dibujaCuadrado(20, 3.5f, 20.001f, 26, 3.5f, 20.001f, 26, 9, 20.001f, 20, 9, 20.001f, 0.9f, 0.4f, 0.2f,1);//base ventana
            dibujaCuadrado(20.3f, 3.8f, 20.01f, 22.9f, 3.8f, 20.01f, 22.9f, 8.7f, 20.01f, 20.3f, 8.7f, 20.01f, 0.0f, 0.5f, 1.0f,1);//cristal izquierdo
            dibujaCuadrado(23.1f, 3.8f, 20.01f, 25.7f, 3.8f, 20.01f, 25.7f, 8.7f, 20.01f, 23.1f, 8.7f, 20.01f, 0.0f, 0.5f, 1.0f,1);//cristal derecho
            
            //BLOQUE MEDIO
            dibujaCuadrado(-4, 0, -28, 5, 0, -28, 5, 12, -28, -4, 12, -28, 1, 1, 0.6f,1);//pared posterior izq
            dibujaCuadrado(5, 0, -24, 18, 0, -24, 18, 12, -24, 5, 12, -24, 1, 1, 0.6f,1);//pared posterior der  
            dibujaCuadrado(-4, 0, -24, -4, 0, -28, -4, 12, -28, -4, 12, -24, 1, 1, 0.6f, 1);//pared izquierda
            dibujaCuadrado(5, 0, -24, 5, 0, -28, 5, 12, -28, 5, 12, -24, 1, 1, 0.6f, 1);//pared derecha
            dibujaCuadrado(-4, 12, -28, -4, 12, 0, 18, 12, 0, 18, 12, -28, 1, 1, 0.6f, 1);//pared superior
            dibujaCuadrado(5, 0.3f, -32, 18, 0.3f, -32, 18, 0.3f, -24, 5, 0.3f, -24, 0.4f, 0.4f, 0.4f, 1);//piso
            dibujaCuadrado(5, 0, -32, 18, 0, -32, 18, 0.3f, -32, 5, 0.3f, -32, 0.4f, 0.4f, 0.5f, 1);//piso frente
            dibujaCuadrado(5, 0, -28, 5, 0, -32, 5, 0.3f, -32, 5, 0.3f, -28, 0.4f, 0.4f, 0.5f, 1);//piso izquierda
            dibujaCuadrado(18, 0, -28, 18, 0, -32, 18, 0.3f, -32, 18, 0.3f, -28, 0.4f, 0.4f, 0.5f, 1);//piso derecha
            dibujaCuadrado(-4, 12, 0, 18, 12, 0, 18, 19, -14, -16, 19, -14, 0.7f, 0.3f, 0.1f, 1);//techo parte delantera
            dibujaCuadrado(-4, 12, -28, 18, 12, -28, 18, 19, -14, -16, 19, -14, 0.7f, 0.3f, 0.1f, 1);//techo parte trasera
            dibujaCuadrado(-4, 0, 0, 18, 0, 0, 18, 12, 0, -4, 12, 0, 1, 1, 0.6f,1);//pared frontal
            //tejado puerta
            dibujaTriangulo(10, 10, 0.001f, 16, 10, 0.001f, 13, 13, 0.001f, 0.7f, 0.3f, 0.1f);//fondo superior
            dibujaTriangulo(10, 10, 3, 16, 10, 3, 13, 13, 3, 0.7f, 0.3f, 0.1f);//frente superior
            dibujaCuadrado(10, 10, 0.001f, 10, 10, 3, 16, 10, 3, 16, 10, 0.001f, 0.7f, 0.3f, 0.1f, 1);//base superior
            dibujaCuadrado(10, 10, 3, 10, 10, 0.001f, 13, 13, 0.001f, 13, 13, 3, 0.7f, 0.2f, 0.0f, 1);//lado izquierdo superior
            dibujaCuadrado(16, 10, 3, 16, 10, 0.001f, 13, 13, 0.001f, 13, 13, 3, 0.7f, 0.2f, 0.0f, 1);//lado derecho superior
            //puerta
            dibujaCuadrado(11, 0.5f, 0.002f, 15, 0.5f, 0.002f, 15, 10, 0.002f, 11, 10, 0.002f, 0.9f, 0.4f, 0.2f, 1);//base
            dibujaCuadrado(12, 5.5f, 0.003f, 12.8f, 5.5f, 0.003f, 12.8f, 9, 0.003f, 12, 9, 0.003f, 0.0f, 0.5f, 1.0f,1);//cristal izquierdo
            dibujaCuadrado(13.2f, 5.5f, 0.003f, 14, 5.5f, 0.003f, 14, 9, 0.003f, 13.2f, 9, 0.003f, 0.0f, 0.5f, 1.0f,1);//cristal derecho
            dibujaCuadrado(12, 3, 0.003f, 14, 3, 0.003f, 14, 3.5f, 0.003f, 12, 3.5f, 0.003f, 0.7f, 0.3f, 0.1f, 1);//adorno inferior          
            
            gl.glPushMatrix();
            gl.glRotated(90, 0, 1, 0);           
            for (int i = 0; i < 4; i++) {                 
                dibujaCilindro(0.5f, 4, 9, 9, 0.9f, 0.1f, 0.0f, 0, -0.5f, 0.5f, 11);//grada
                gl.glTranslatef(-0.5f, 0 , 0);  
            }
            gl.glPopMatrix();
          
            //ventana frente
            gl.glPushMatrix();
                gl.glTranslatef(0, 1, 0);
                dibujaCuadrado(-2.1f, 3, 0.001f, 7.1f, 3, 0.001f, 7.1f, 9, 0.001f, -2.1f, 9, 0.001f, 0.9f, 0.4f, 0.2f, 1);//base ventana
                //cristales ventana
                gl.glPushMatrix();
                for (int i = 0; i < 3; i++) {               
                    dibujaCuadrado(-1.8f, 7.1f, 0.003f, 0.9f, 7.1f, 0.003f, 0.9f, 8.8f, 0.003f, -1.9f, 8.8f, 0.003f,0.0f, 0.5f, 1.0f, 1);//fila superior
                    gl.glTranslatef(3, 0, 0);
                }
                gl.glPopMatrix();

                gl.glPushMatrix();            
                for (int i = 0; i < 3; i++) {               
                    dibujaCuadrado(-1.8f, 5.1f, 0.003f, 0.9f, 5.1f, 0.003f, 0.9f, 6.9f, 0.003f, -1.9f, 6.9f, 0.003f,0.0f, 0.5f, 1.0f, 1);//fila media
                    gl.glTranslatef(3, 0, 0);
                }
                gl.glPopMatrix();

                gl.glPushMatrix();
                for (int i = 0; i < 3; i++) {               
                    dibujaCuadrado(-1.8f, 3.2f, 0.003f, 0.9f, 3.2f, 0.003f, 0.9f, 4.9f, 0.003f, -1.9f, 4.9f, 0.003f,0.0f, 0.5f, 1.0f, 1);//fila inferior
                    gl.glTranslatef(3, 0, 0);
                }
                gl.glPopMatrix();
            gl.glPopMatrix();
            
            //POSTERIOR MEDIO   
            //tejado parte posterior
            dibujaCuadrado(5, 11.7f, -32, 18, 11.7f, -32, 18, 11.7f, -24, 5, 11.7f, -24, 0.7f, 0.3f, 0.1f, 1);//parte inferior
            dibujaCuadrado(5, 12.1f, -32, 18, 12.1f, -32, 18, 12.1f, -24, 5, 12.1f, -24, 0.7f, 0.2f, 0.0f, 1);//parte superior
            dibujaCuadrado(5, 12.1f, -32, 18, 12.1f, -32, 18, 11.7f, -32, 5, 11.7f, -32, 0.7f, 0.3f, 0.1f, 1);//tejado frente
            dibujaCuadrado(5, 12.1f, -28, 5, 12.1f, -32, 5, 11.7f, -32, 5, 11.7f, -28, 0.7f, 0.3f, 0.1f, 1);//tejado izquierda
            dibujaCuadrado(18, 12.1f, -28, 18, 12.1f, -32, 18, 11.7f, -32, 18, 11.7f, -28, 0.7f, 0.3f, 0.1f, 1);//tejado derecha
            //columnas parte posterior
            gl.glPushMatrix();
            for (int i = 0; i < 3; i++) {               
                dibujaCilindro(0.3f, 11.7f, 20, 20, 0.7f, 0.2f, 0.0f, -90, 5.6f, 0.3f, -31.4f);
                gl.glTranslatef(6f, 0, 0);
            }
            gl.glPopMatrix();           
        }   
}
