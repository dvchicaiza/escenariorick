/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dchicaiza;
import com.sun.opengl.util.GLUT;
import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import javax.media.opengl.GL;
/**
 *
 * @author Doris
 */
public class ExtGarage {
    GL gl;
    GLUT glut;
    
    public ExtGarage(GL gl, GLUT glut){
        this.gl=gl; 
        this.glut=glut;
    }
    
    public void dibujaCuadrado(float x1, float y1,float z1, float x2, float y2, float z2, float x3, float y3, float z3, float x4, float y4,float z4, float r, float g, float b){
        gl.glPushMatrix();    
        gl.glColor3f(r, g, b);
            gl.glBegin(GL.GL_QUADS);
            gl.glVertex3f(x1, y1, z1);
            gl.glVertex3f(x2, y2, z2);
            gl.glVertex3f(x3, y3, z3);
            gl.glVertex3f(x4, y4, z4);
            gl.glEnd();
        gl.glPopMatrix();
        }
        
        void dibujaTriangulo(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3,float z3, float r, float g, float b){
            gl.glPushMatrix();
            gl.glColor3f(r,g,b);
            gl.glBegin(GL.GL_TRIANGLES);
            gl.glVertex3f(x1, y1, z1);
            gl.glVertex3f(x2, y2, z2);
            gl.glVertex3f(x3, y3, z3);
            gl.glEnd(); 
            gl.glPopMatrix();
        }
        
        void dibujaMedioCirculo(float posx, float posy, float posz, float radio, float r, float g, float b){
            gl.glPushMatrix();
            gl.glColor3f(r, g, b);
            gl.glBegin(GL.GL_POLYGON);
            for (float i = 0; i < 180; i++)//va de [0, 360]grados
            {
                float x,y;
                float theta=(float) (i*PI/180);//conversion de grados a radianes
                //conversion de coordenadas polares a rectangulares
                x=(float)(radio*cos(theta));
                y=(float)(radio*sin(theta));
                gl.glVertex3f(x+posx, y+posy, posz);
            }
            gl.glEnd();
            gl.glPopMatrix();
        }
        
        void dibujarTorus(double ri, double re, int sl, int st, float r, float g, float b, float angle, float PX, float PY, float PZ) {
            gl.glPushMatrix();
            gl.glTranslatef(PX, PY, PZ);
            gl.glRotated(angle, 1, 0, 0);
            gl.glColor3f(r, g, b);
            glut.glutWireTorus(ri, re, sl, st);
            gl.glPopMatrix();
        }
        
        public void dibujarExtGarage(){
            dibujaCuadrado(-28, 0, -24.02f, -4, 0, -24.02f, -4, 12, -24.02f, -28, 12, -24.02f, 1, 0.94f, 0.6f);//pared posterior
            dibujaTriangulo(-28, 12, -24.02f, -4, 12, -24.02f, -16, 19, -24.02f, 1, 0.94f, 0.6f);//pared posterior superior
            dibujaCuadrado(-28, 0, 10, -28, 0, -24, -28, 12, -24, -28, 12, 10, 0, 1, 0);//pared izquierda
            dibujaCuadrado(-4, 0, 10, -4, 0, -24, -4, 12, -24, -4, 12, 10, 0, 0, 0.6f);//pared derecha            
            dibujaTriangulo(-28, 12, 10, -4, 12, 10, -16, 19, 10, 1f, 0.94f, 0.6f);//pared frontal superior
            //aro
            gl.glPushMatrix();
            dibujaMedioCirculo(-16, 13.5f, 10.1f, 3, 1, 1, 1);//fondo arriba
            gl.glPopMatrix();
            
            dibujaCuadrado(-19, 12, 10.1f, -13, 12, 10.1f, -13, 13.6f, 10.1f, -19, 13.6f, 10.1f, 1, 1, 1);//fondo abajo
            dibujaCuadrado(-17.5f, 12, 10.2f, -14.5f, 12, 10.2f, -14.5f, 14, 10.2f, -17.5f, 14, 10.2f, 1, 0, 0);//base aro
            dibujarTorus(0.3f, 0.5f, 8, 8, 0.4f, 0.4f, 0.4f, 90, -15.8f, 13.1f, 10.9f);
            dibujarTorus(0.2f, 0.4f, 5, 5, 0.4f, 0.4f, 0.4f, 90, -15.8f, 12.6f, 10.9f);
            dibujaCuadrado(-28, 0, 10, -26, 0, 10, -26, 11, 10, -28, 11, 10, 1f, 0.94f, 0.6f);//frontal inf izq
            dibujaCuadrado(-6, 0, 10, -4, 0, 10, -4, 11, 10, -6, 11, 10, 1f, 0.94f, 0.6f);//frontal inf der
            dibujaCuadrado(-28, 11, 10, -4, 11, 10, -4, 12, 10, -28, 12, 10, 1f, 0.94f, 0.6f);//frontal sup
            dibujaCuadrado(-4, 12, 10, -4, 12, -24, -16, 19, -24, -16, 19, 10, 0.6f, 0.3f, 0.1f);//techo parte derecha
            dibujaCuadrado(-28, 12, 10, -28, 12, -24, -16, 19, -24, -16, 19, 10, 0.6f, 0.3f, 0.1f);//techo parte izquierda
            
            //ventana derecha
            dibujaCuadrado(-3.99f, 4, 6.5f, -3.99f, 4, 3.5f, -3.99f, 9, 3.5f, -3.99f, 9, 6.5f, 0.9f, 0.4f, 0.2f);//base
            dibujaCuadrado(-3.98f, 4.2f, 6.3f, -3.98f, 4.2f, 3.7f, -3.98f, 6.4f, 3.7f, -3.98f, 6.4f, 6.3f, 0.0f, 0.5f, 1.0f);//cristal inferior
            dibujaCuadrado(-3.98f, 6.6f, 6.3f, -3.98f, 6.6f, 3.7f, -3.98f, 8.8f, 3.7f, -3.98f, 8.8f, 6.3f, 0.0f, 0.5f, 1.0f);//cristal superior
        }
}
        
