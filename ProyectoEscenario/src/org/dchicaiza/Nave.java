/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dchicaiza;
import com.sun.opengl.util.GLUT;
import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
/**
 *
 * @author Doris
 */
public class Nave {
    GL gl;
    GLUT glut;
    GLU glu;
  
    public Nave(GL gl, GLUT glut, GLU glu){
        this.gl=gl; 
        this.glut=glut;
        this.glu=glu;      
    }
 
    public void dibujarEsfera(double R, int sl, int st, float r, float g, float b, float t, float PX, float PY, float PZ) {
        gl.glPushMatrix();
        gl.glTranslatef(PX,PY,PZ);
        gl.glColor4f(r,g,b,t);
        glut.glutSolidSphere(R, sl, st);
        gl.glPopMatrix();
    }
    
    public void dibujarCilindro(GLUquadric quad,float B, float TP, float A, int sl, int st,float r, float g, float b,float PX, float PY, float PZ,float angx,float angy){        
        gl.glPushMatrix();        
        gl.glTranslatef(PX,PY,PZ);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotatef(angy, 0, 1, 0);
        gl.glColor3f(r,g,b);
        glu.gluCylinder(quad, B, TP, A, sl, st);
        gl.glPopMatrix();
    }
    
    public void dibujarCilindroGlut(double R, double A, int sl, int st, float r, float g, float b, float PX, float PY, float PZ, float angx,float angy) {
            gl.glPushMatrix();
            gl.glTranslatef(PX,PY,PZ);
            gl.glRotated(angx, 1, 0, 0);
            gl.glRotated(angy, 0, 1, 0);
            gl.glColor3f(r,g,b);
            glut.glutSolidCylinder(R,A,sl,st);
            gl.glPopMatrix();
        }
    
    public void dibujarTorus(double ri, double re, int sl, int st, float r, float g, float b, float angle, float PX, float PY, float PZ) {
            gl.glPushMatrix();
            gl.glTranslatef(PX, PY, PZ);
            gl.glRotated(angle, 1, 0, 0);
            gl.glColor3f(r, g, b);
            glut.glutSolidTorus(ri, re, sl, st);
            gl.glPopMatrix();
        }
    
    public void dibujarCirculo(float posx, float posy, float posz, float radio, float r, float g, float b){
            gl.glPushMatrix();           
            gl.glColor3f(r, g, b);
            gl.glBegin(GL.GL_POLYGON);
            for (float i = 0; i < 360; i++)//va de [0, 360]grados
            {
                float x,y;
                float theta=(float) (i*PI/180);//conversion de grados a radianes
                //conversion de coordenadas polares a rectangulares
                x=(float)(radio*cos(theta));
                y=(float)(radio*sin(theta));
                gl.glVertex3f(x+posx, y+posy, posz);
            }
            gl.glEnd();
            gl.glPopMatrix();
        }
    
    public void dibujarNave(){
        GLUquadric quad;
        quad=glu.gluNewQuadric();
        
        gl.glPushMatrix();
        dibujarEsfera(3, 28, 12, 0.5f, 0.5f, 0.5f, 0.9f, 3, 3, 25);//base
        gl.glPopMatrix();

        dibujarCilindro(quad, 3, 5, 1.2f, 30, 10, 0.4f, 0.4f, 0.4f, 3, 3f, 25,90,0);//anillo
        
        gl.glPushMatrix();
        glu.gluQuadricDrawStyle(quad,GLU.GLU_LINE);
        gl.glLineWidth(2);
        dibujarCilindro(quad, 3, 5, 1.2f, 7, 0, 1, 0.5f, 0, 3, 3f, 25,90,0);//anillo wire
        gl.glPopMatrix();

        gl.glPushMatrix();
        gl.glTranslatef(3, 1.8f, 25);
        gl.glRotatef(90, 1, 0, 0);
        dibujarCirculo(0, 0, 0, 5f, 0.4f, 0.4f, 0.4f);//base anillo     
        gl.glPopMatrix();
        
        //ruedas
        dibujarTorus(0.25f, 0.6f, 18, 18, 0, 0, 0, 0, 1.2f, 0.95f, 27);//frente exterior izquierda
        dibujarTorus(0.25f, 0.6f, 18, 18, 0, 0, 0, 0, 1.2f, 0.95f, 22.8f);//trasera exterior izquierda 
        dibujarTorus(0.25f, 0.6f, 18, 18, 0, 0, 0, 0, 5, 0.95f, 22.8f);//trasera exterior derecha
        dibujarTorus(0.25f, 0.6f, 18, 18, 0, 0, 0, 0, 5f, 0.95f, 27);//frente exterior derecha
        dibujarEsfera(0.4f, 10, 10, 0.4f, 0.4f, 0.4f, 1, 1.2f, 0.95f, 22.8f);//trasera interior izquierda
        dibujarEsfera(0.4f, 10, 10, 0.4f, 0.4f, 0.4f, 1, 5, 0.95f, 22.8f);//trasera interior derecha
        dibujarEsfera(0.4f, 10, 10, 0.4f, 0.4f, 0.4f, 1, 1.2f, 0.95f, 27);//frente interior izquierda
        dibujarEsfera(0.4f, 10, 10, 0.4f, 0.4f, 0.4f, 1, 5f, 0.95f, 27);//frente interior derecha
        
        //tanques
        //tanque fondo
        dibujarCilindro(quad, 0.6f, 0.6f, 2, 12, 9, 0.3f, 0.3f, 0.3f, -1, 3.5f, 21,0,90);//fondo exterior
        dibujarCilindroGlut(0.6f, 2, 9, 9, 0, 0, 0, -1, 3.5f, 21,0,90);//fondo interior
        dibujarCilindroGlut(0.2f, 2, 9, 9, 0.5f, 0.6f, 0.5f, 0.3f, 3.5f, 21, 30, 0);//manguera fondo
        //tanque frente
        dibujarCilindro(quad, 0.6f, 0.6f, 2, 12, 9, 0.3f, 0.3f, 0.3f, -1, 3.5f, 29,0,90);//frente exterior
        dibujarCilindroGlut(0.6f, 2, 9, 9, 0, 0, 0, -1, 3.5f, 29,0,90);//frente interior
        dibujarCilindroGlut(0.2f, 2, 9, 9, 0.5f, 0.6f, 0.5f, 0.3f, 2.5f, 27, -30, 0);//manguera frente  
        
        //luces
        gl.glPushMatrix();
        glu.gluQuadricDrawStyle(quad,GLU.GLU_FILL);
        dibujarCilindro(quad, 0.2f, 0.2f, 0.6f, 60, 60, 0.5f, 0.7f, 0.5f, 6.5f, 2.4f, 23,0,90);//fondo exterior
        dibujarCilindro(quad, 0.2f, 0.2f, 0.6f, 60, 60, 0.5f, 0.7f, 0.5f, 6.5f, 2.4f, 27,0,90);//frente exterior
        gl.glPopMatrix();
        dibujarCilindroGlut(0.2f, 0.6f, 9, 9, 1, 1, 0, 6.5f, 2.4f, 23,0,90);//fondo interior
        dibujarCilindroGlut(0.2f, 0.6f, 9, 9, 1, 1, 0, 6.5f, 2.4f, 27,0,90);//frente interior
        
    }
}
