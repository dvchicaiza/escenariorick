/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dchicaiza;
import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
/**
 *
 * @author Doris
 */
public class DetallePatio {
    GL gl;
    GLUT glut;
    
    public DetallePatio(GL gl, GLUT glut){
        this.gl=gl; 
        this.glut=glut;
    }
    
        public void dibujaCilindro(double R, double A, int sl, int st, float r, float g, float b, float angle, float PX, float PY, float PZ) {
            gl.glPushMatrix();
            gl.glTranslatef(PX,PY,PZ);
            gl.glRotated(angle, 1, 0, 0);
            gl.glColor3f(r,g,b);
            glut.glutSolidCylinder(R,A,sl,st);
            gl.glPopMatrix();
        }
        
        public void dibujaCono(double B, double A, int sl, int st, float r, float g, float b, float angle, float PX, float PY, float PZ) {
            gl.glPushMatrix();
            gl.glTranslatef(PX, PY, PZ);
            gl.glRotated(angle, 1, 0, 0);
            gl.glColor3f(r, g, b);
            glut.glutSolidCone(B, A, sl, st);
            gl.glPopMatrix();
        }
        
        public void dibujaCuadrado(float x1, float y1,float z1, float x2, float y2, float z2, float x3, float y3, float z3, float x4, float y4,float z4, float r, float g, float b,float t){
            gl.glPushMatrix();
            gl.glColor4f(r, g, b,t);
            gl.glBegin(GL.GL_QUADS);
            gl.glVertex3f(x1, y1, z1);
            gl.glVertex3f(x2, y2, z2);
            gl.glVertex3f(x3, y3, z3);
            gl.glVertex3f(x4, y4, z4);
            gl.glEnd();
            gl.glPopMatrix();
        }

        public void dibujaEsfera(double R, int sl, int st, float r, float g, float b, float PX, float PY, float PZ) {
            gl.glPushMatrix();
            gl.glTranslatef(PX,PY,PZ);
            gl.glColor3f(r,g,b);
            glut.glutSolidSphere(R, sl, st);
            gl.glPopMatrix();
        }
        
        public void dibujaDodecaedro(float r, float g, float b, float PX, float PY, float PZ) {
            gl.glPushMatrix();
            gl.glTranslatef(PX,PY,PZ);
            gl.glColor3f(r,g,b);
            glut.glutSolidDodecahedron();
            gl.glPopMatrix();
        }
        
        public void dibujarDetallePatio(){
            //cerramiento jardin     
            gl.glPushMatrix();
            for (int i = 0; i < 4; i++) {               
                dibujaCilindro(0.3f, 3.5f, 9, 9, 0.9f, 0.4f, 0.2f, 0, 18.15f, 0.3f, 20);//izquierda
                gl.glTranslatef(0, 0.5f, 0);
            }
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            for (int i = 0; i < 4; i++) {               
                dibujaCilindro(0.3f, 3.5f, 9, 9, 0.9f, 0.4f, 0.2f, 0, 27.85f, 0.3f, 20);//derecha
                gl.glTranslatef(0, 0.5f, 0);
            }
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            gl.glRotated(90, 0, 1, 0);
            for (int i = 0; i < 4; i++) {                 
                dibujaCilindro(0.3f, 10f, 9, 9, 0.9f, 0.4f, 0.2f, 0, -23.7f, 0.3f, 18f);//frente
                gl.glTranslatef(0, 0.5f, 0);  
            }
            gl.glPopMatrix();
            
            //dibuja hierbitas
            gl.glPushMatrix();
            for (int i = 0; i < 5; i++) {                 
                dibujaCono(1.1f, 4, 8, 8, 0, 1, 0, -90, 19.4f, 0, 20.8f);//fila fondo
                gl.glTranslatef(1.8f, 0, 0);  
            }
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            for (int i = 0; i < 7; i++) {                 
                dibujaCono(0.78f, 3, 8, 8, 0, 1, 0, -90, 19.2f, 0, 23f);//fila frente
                gl.glTranslatef(1.3f, 0, 0);  
            }
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            for (int i = 0; i < 9; i++) {                 
                dibujaCono(0.67f, 3.5f, 8, 8, 0, 1, 0, -90, 19f, 0, 22f);//fila medio
                gl.glTranslatef(1, 0, 0);  
            }
            gl.glPopMatrix();
            
            //camino salida puerta
            gl.glPushMatrix();          
            for (int i = 0; i < 14; i++) { 
                dibujaCilindro(0.3f, 2f, 9, 9, 0.9f, 0.4f, 0.2f, 0, 10.7f, 0.3f, 0);//borde izquierdo
                gl.glTranslatef(0, 0 , 2.05f);  
            }
            gl.glPopMatrix();
            
            gl.glPushMatrix();          
            for (int i = 0; i < 14; i++) { 
                dibujaCilindro(0.3f, 2f, 9, 9, 0.9f, 0.4f, 0.2f, 0, 15.3f, 0.3f, 0);//borde derecho
                gl.glTranslatef(0, 0 , 2.05f);  
            }
            gl.glPopMatrix();
            //base camino
            dibujaCuadrado(10.8f, 0, 2, 10.8f, 0, 28.5f, 15.3f, 0, 28.5f, 15.3f, 0, 2, 1, 1, 1, 1);
            
            //planta lado derecho garage
            dibujaCilindro(1, 2, 10, 10, 1, 0.4f, 0.2f, -90, -2.8f, 0, 8);//maceta
            dibujaEsfera(1.2f, 10, 10, 0, 1, 0, -2.7f, 3, 8);//planta
            
            //plantas frente bloque medio
            dibujaDodecaedro(0, 1, 0, 7, 1.7f , 1.8f);//media
            
            gl.glPushMatrix();
            gl.glScalef(0.8f, 0.7f, 0.7f);
            dibujaDodecaedro(0, 1, 0, 7, 1.7f , 1.8f);//izquierda
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            gl.glScalef(0.8f, 0.5f, 0.6f);
            dibujaDodecaedro(0, 1, 0, 11, 1.7f , 2);//derecha
            gl.glPopMatrix();
            
            //salida garage
            dibujaCuadrado(-28, 0, 10, -28, 0, 28.5f, -4, 0, 28.5f, -4, 0, 10, 0.98f, 0.98f, 0.98f, 1);
        }
}
