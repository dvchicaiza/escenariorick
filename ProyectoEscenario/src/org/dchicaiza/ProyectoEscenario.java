package org.dchicaiza;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;

// Librerias para teclas
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * ProyectoEscenario.java 
 * Este proyecto crea el Escenario de Rick(Garage con detalles, nave y exterior casa)
 * Autor: Doris Chicaiza
 *
 */
public class ProyectoEscenario implements GLEventListener, KeyListener {
     //Variables para la camara
    float transx = 0, transy = 0;
    int cam = 1;
    float radio = 20, angulo = 0;
    
    // Variables para las transformaciones del avatar
    private static float trasladaX = 0;
    private static float trasladaY = 0;
    private static float trasladaZ = 0;
    public static void main(String[] args) {
        Frame frame = new Frame("Escenario Rick");
        GLCanvas canvas = new GLCanvas();

        canvas.addGLEventListener(new ProyectoEscenario());
        frame.add(canvas);
        frame.setSize(1400, 700);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
    }

    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
        drawable.addKeyListener(this);
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        if (height <= 0) { // avoid a divide by zero error!
        
            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(70.0f, h, 1.0, 200.0);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();
        GLUT glut = new GLUT();       
        // Clear the drawing area       
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);//limpia buffer de color y de profundidad
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();
        gl.glEnable(GL.GL_DEPTH_TEST);//para activar profundidad
        gl.glEnable(GL.GL_BLEND);//se habilita para usar transparencia de color
        gl.glBlendFunc(GL.GL_SRC_ALPHA,GL.GL_ONE_MINUS_SRC_ALPHA); //con esta funcion se logra transparencia en los objetos        
        gl.glTranslatef(0, 0, -60);
        gl.glRotatef(20, 1, 0, 0);

        //CAMARAS
        //Camara que sigue al avatar(nave)
        glu.gluLookAt(-0.2f + trasladaX, -0.7f , 5.5 + trasladaZ,0.5+trasladaX, 0,2.5+trasladaZ,0.0, 1.0, 0.0);
        //camara estatica , vista frontal
        if (cam == 1) {
            glu.gluLookAt(0, 0, 0.2f, 0, 0, 0, 0, 1, 0);          
        }
        
        // camara circular que rota alrededor del eje Y
        if (cam == 2) {
            glu.gluLookAt((radio * Math.sin(angulo)), 1, (radio * Math.cos(angulo)), 0, 0, 0, 0, 1, 0);
        }

        //circular radio variable, sensacion de que se aleja y luego se acerca
        if (cam == 3) {
            glu.gluLookAt(((radio - 5) * Math.sin(angulo)) + 25, 5, ((radio - 5) * Math.cos(angulo)) - 15, 25, 0, -15, 0, 1, 0);
        }
        
        //vista posterior, donde cambia el sentido de la rotacion
        if (cam == 4) {
            glu.gluLookAt(((radio - 5) * Math.sin(angulo)), 10, ((radio - 5) * Math.cos(angulo)) - 11, 0, 0, 8, 0, 1, 0);;
        }
        
        //Vista desde arriba con un grado de angulacion
        if (cam == 5) {
            glu.gluLookAt(25, 40, - 11, 0, 0, 8, 0, 1, 0);
        }
                      
            //dibuja piso           
            gl.glPushMatrix();         
            gl.glColor3f(0, 0.4f, 0.2f);
            gl.glBegin(GL.GL_QUADS);
                    gl.glVertex3f(-35, -0.01f, 35);
                    gl.glVertex3f(35, -0.01f, 35);
                    gl.glVertex3f(35, -0.01f, -35);
                    gl.glVertex3f(-35, -0.01f, -35);
            gl.glEnd();
            gl.glPopMatrix();
       
            gl.glPushMatrix();
            Casa casa = new Casa(gl, glut);
            casa.dibujarCasa();
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            DetallePatio dp = new DetallePatio(gl, glut);
            dp.dibujarDetallePatio();
            gl.glPopMatrix();
                     
            gl.glPushMatrix();
            gl.glScalef(0.6f, 0.6f, 0.6f);
            gl.glTranslatef(-28, 0, 19);
            gl.glTranslatef(trasladaX, trasladaY, trasladaZ);//traslada el avatar en los ejes x,y,z
            Nave nave = new Nave(gl, glut,glu);
            nave.dibujarNave();
            gl.glPopMatrix();

            gl.glPushMatrix();
            ExtGarage eg = new ExtGarage(gl, glut);
            eg.dibujarExtGarage();
            gl.glPopMatrix();
            
            gl.glPushMatrix();
            IntGarage ig = new IntGarage(gl, glut, glu);
            ig.dibujarIntGarage();
            gl.glPopMatrix();
            
        // Flush all drawing operations to the graphics card
        gl.glFlush();
        
        angulo = angulo + 0.013f;
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyPressed(KeyEvent e) {     
        //Eventos de teclado para traslacion del avatar 
        if(e.getKeyCode()== KeyEvent.VK_RIGHT){
            if(trasladaX<65){
                trasladaX+=0.15f;
            }else{
                trasladaX+=0;
            }
               
            System.out.println("Valor en la traslacion de X: "+trasladaX);
        }
        
        if(e.getKeyCode()== KeyEvent.VK_LEFT){
            if(trasladaX>-30){
                trasladaX-=0.15f;
            }else{
                trasladaX-=0;
            }
            System.out.println("Valor en la traslacion de X: "+trasladaX);
        }
        
        if(e.getKeyCode()== KeyEvent.VK_Y){
            if(trasladaY<26){
                trasladaY+=0.2f;
            }else{
                trasladaY+=0;
            }
               
            System.out.println("Valor en la traslacion de Y: "+trasladaY);
        }
        
        if(e.getKeyCode()== KeyEvent.VK_B){
            if(trasladaY>0.01f){
                trasladaY-=0.2f;
            }else{
                trasladaY-=0;
            }
            System.out.println("Valor en la traslacion de Y: "+trasladaY);
        }
        
        if(e.getKeyCode()== KeyEvent.VK_UP){
            if(trasladaZ<11){
                trasladaZ+=0.2f;
            }else{
                trasladaZ+=0;
            }
            System.out.println("Valor en la traslacion de Z: "+trasladaZ);
        }
        
        if(e.getKeyCode()== KeyEvent.VK_DOWN){
            if(trasladaZ>-92){
                trasladaZ-=0.2f;;
            }else{
                trasladaZ-=0;
            }
            System.out.println("Valor en la traslacion de Z: "+trasladaZ);
        }
       
         if(e.getKeyCode()== KeyEvent.VK_ESCAPE){
            trasladaX=0;
            trasladaY=0;
            trasladaZ=0;
            radio = 20;
        }       

        //Eventos de teclado para las camaras
        if (e.getKeyCode() == KeyEvent.VK_1) {
            cam = 1;

        }
        if (e.getKeyCode() == KeyEvent.VK_2) {
            cam = 2;

        }
        if (e.getKeyCode() == KeyEvent.VK_3) {
            cam = 3;

        }
        if (e.getKeyCode() == KeyEvent.VK_4) {
            cam = 4;

        }
        if (e.getKeyCode() == KeyEvent.VK_5) {
            cam = 5;

        }

        //varian el radio cuando cuando una camara con rotacion esta activa
        if (e.getKeyCode() == KeyEvent.VK_P) {
            radio = radio + 1.5f;

        }
        if (e.getKeyCode() == KeyEvent.VK_L) {
            radio = radio - 1.5f;

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }
}


