/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dchicaiza;
import com.sun.opengl.util.GLUT;
import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
/**
 *
 * @author Doris
 */
public class IntGarage {
    GL gl;
    GLUT glut;
    GLU glu;
  
    public IntGarage(GL gl, GLUT glut, GLU glu){
        this.gl=gl; 
        this.glut=glut;
        this.glu=glu;      
    } 
    
    public void dibujarCubo(float L, float r, float g, float b, float PX, float PY, float PZ, float angx){
        gl.glPushMatrix();
        gl.glTranslated(PX,PY,PZ);
        gl.glRotated(angx, 1, 0, 0);
        gl.glColor3f(0.7f, 0.7f, 0.7f);
        glut.glutWireCube(L);
        gl.glColor3f(r, g, b);
        glut.glutSolidCube(L);
        gl.glPopMatrix();
        }
    
    public void dibujaCuadrado(float x1, float y1,float z1, float x2, float y2, float z2, float x3, float y3, float z3, float x4, float y4,float z4, float r, float g, float b){
            gl.glPushMatrix();
            gl.glColor3f(r, g, b);
            gl.glBegin(GL.GL_QUADS);
            gl.glVertex3f(x1, y1, z1);
            gl.glVertex3f(x2, y2, z2);
            gl.glVertex3f(x3, y3, z3);
            gl.glVertex3f(x4, y4, z4);
            gl.glEnd();
            gl.glPopMatrix();
        }
        
        void dibujaTriangulo(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3,float z3, float r, float g, float b){
            gl.glPushMatrix();
            gl.glColor3f(r,g,b);
            gl.glBegin(GL.GL_TRIANGLES);
            gl.glVertex3f(x1, y1, z1);
            gl.glVertex3f(x2, y2, z2);
            gl.glVertex3f(x3, y3, z3);
            gl.glEnd();
            gl.glPopMatrix();
        }
        
        void dibujarLineaCerrada(float x1, float y1,float z1, float x2, float y2, float z2, float x3, float y3, float z3, float x4, float y4,float z4, float r, float g, float b){
            gl.glPushMatrix();
            gl.glColor3f(r, g, b);
            gl.glBegin(GL.GL_LINE_LOOP);
            gl.glVertex3f(x1, y1, z1);
            gl.glVertex3f(x2, y2, z2);
            gl.glVertex3f(x3, y3, z3);
            gl.glVertex3f(x4, y4, z4);
            gl.glEnd();
            gl.glPopMatrix();
        }
        
        void dibujarLinea(float x1, float y1,float z1, float x2, float y2, float z2,float r, float g, float b){
            gl.glPushMatrix();
            gl.glColor3f(r, g, b);
            gl.glBegin(GL.GL_LINES);
            gl.glVertex3f(x1, y1, z1);
            gl.glVertex3f(x2, y2, z2);
            gl.glEnd();
            gl.glPopMatrix();
        }
        
        public void dibujarCirculo(float posx, float posy, float posz, float radio, float r, float g, float b){
            gl.glPushMatrix();           
            gl.glColor3f(r, g, b);
            gl.glBegin(GL.GL_POLYGON);
            for (float i = 0; i < 360; i++)//va de [0, 360]grados
            {
                float x,y;
                float theta=(float) (i*PI/180);//conversion de grados a radianes
                //conversion de coordenadas polares a rectangulares
                x=(float)(radio*cos(theta));
                y=(float)(radio*sin(theta));
                gl.glVertex3f(x+posx, y+posy, posz);
            }
            gl.glEnd();
            gl.glPopMatrix();
        }
        
        public void dibujarCilindroGlut(double R, double A, int sl, int st, float r, float g, float b, float PX, float PY, float PZ, float angx,float angy,float angz) {
            gl.glPushMatrix();
            gl.glTranslatef(PX,PY,PZ);
            gl.glRotated(angx, 1, 0, 0);
            gl.glRotated(angy, 0, 1, 0);
            gl.glRotated(angz, 0, 0, 1);
            gl.glColor3f(0.7f, 0.7f, 0.7f);
            glut.glutWireCylinder(R,A,sl,st);
            gl.glColor3f(r,g,b);
            glut.glutSolidCylinder(R,A,sl,st);
            gl.glPopMatrix();
        }
        
        public void dibujarCilindro(GLUquadric quad,float B, float TP, float A, int sl, int st,float r, float g, float b,float PX, float PY, float PZ,float angx,float angy){        
        gl.glPushMatrix();        
        gl.glTranslatef(PX,PY,PZ);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotatef(angy, 0, 1, 0);
        gl.glColor3f(r,g,b);
        glu.gluCylinder(quad, B, TP, A, sl, st);
        gl.glPopMatrix();
    }
  
    public void dibujarIntGarage(){    
        GLUquadric quad;
        quad=glu.gluNewQuadric();
        
        //muebles 
        //bloque fondo
        dibujaCuadrado(-23, 0, -19, -16, 0, -19, -16, 4, -19, -23, 4, -19, 0.3f, 0.3f, 0.3f);//frente inferior
        dibujaCuadrado(-23, 4, -19, -16, 4, -19, -16, 4.3f, -19, -23, 4.3f, -19, 0.85f, 0.85f, 0.85f);//frente superior
        dibujaCuadrado(-16, 0, -19, -16, 0, -24, -16, 4, -24, -16, 4, -19, 0.3f, 0.3f, 0.3f);//derecho
        dibujaCuadrado(-16, 4, -19, -16, 4, -24, -16, 4.3f, -24, -16, 4.3f, -19, 0.85f, 0.85f, 0.85f);//derecho superior
        dibujaCuadrado(-28, 4, -24, -28, 4, -19, -16, 4, -19, -16, 4, -24, 0.95f, 0.95f, 0.95f);//base superior
        dibujaCuadrado(-28, 4.3f, -24, -28, 4.3f, -19, -16, 4.3f, -19, -16, 4.3f, -24, 0.95f, 0.95f, 0.95f);//superior
            //cajones
            dibujarCubo(3, 0.5f, 0.5f, 0.5f, -21.2f, 2, -20.4f, 0);
            dibujarCubo(3, 0.5f, 0.5f, 0.5f, -17.8f, 2, -20.4f,0);
            //divisiones
            dibujarLinea(-21.2f, 0.5f, -18.85f, -21.2f, 3.5f, -18.85f, 0.35f, 0.35f, 0.35f);
            dibujarLinea(-17.8f, 0.5f, -18.85f, -17.8f, 3.5f, -18.85f, 0.35f, 0.35f, 0.35f);
            //manijas
            dibujarCilindroGlut(0.08f, 0.06f, 10, 10, 1, 1, 1, -21.5f, 3, -18.9f, 0, 0,0);//izq1
            dibujarCilindroGlut(0.08f, 0.06f, 10, 10, 1, 1, 1, -20.9f, 3, -18.9f, 0, 0,0);//izq2
            dibujarCilindroGlut(0.08f, 0.06f, 10, 10, 1, 1, 1, -18.1f, 3, -18.9f, 0, 0,0);//der1
            dibujarCilindroGlut(0.08f, 0.06f, 10, 10, 1, 1, 1, -17.5f, 3, -18.9f, 0, 0,0);//der2
        
        //bloque izquierda
        dibujaCuadrado(-23, 0, 0, -23, 0, -19, -23, 4, -19, -23, 4, 0, 0.3f, 0.3f, 0.3f);//derecho
        dibujaCuadrado(-23, 4, -5, -23, 4, -19, -23, 4.3f, -19, -23, 4.3f, -5, 0.85f, 0.85f, 0.85f);//derecho superior
        dibujaCuadrado(-28, 0, 0, -23, 0, 0, -23, 4, 0, -28, 4, 0, 0.3f, 0.3f, 0.3f);//frente
        dibujaCuadrado(-28, 4, -19, -28, 4, -5, -23, 4, -5, -23, 4, -19,0.95f, 0.95f, 0.95f);//base superior
        dibujaCuadrado(-28, 4.3f, -19, -28, 4.3f, -5, -23, 4.3f, -5, -23, 4.3f, -19, 0.95f, 0.95f, 0.95f);//superior
            //cajones
            gl.glPushMatrix();        
            for (int i = 0; i < 3; i++) {                 
                dibujarCubo(3, 0.5f, 0.5f, 0.5f, -24.4f, 2, -16.3f,0);//
                gl.glTranslatef(0, 0 , 4.2f);  
            }
            gl.glPopMatrix();
            //divisiones
            gl.glPushMatrix();        
            for (int i = 0; i < 3; i++) {                 
                dibujarLinea(-22.85f, 0.5f, -16.3f, -22.85f, 3.5f, -16.3f, 0.35f, 0.35f, 0.35f);
                gl.glTranslatef(0, 0 , 4.2f);  
            }
            gl.glPopMatrix();
            //manijas
            gl.glPushMatrix();        
            for (int i = 0; i < 3; i++) {                 
                dibujarCilindroGlut(0.08f, 0.06f, 12, 1, 1, 1, 1, -22.9f, 3, -16.6f, 0, 0, 0);//fondo1
                dibujarCilindroGlut(0.08f, 0.06f, 12, 1, 1, 1, 1, -22.9f, 3, -16, 0, 0,0);//fondo2
                gl.glTranslatef(0, 0 , 4.2f);  
            }
            gl.glPopMatrix();
        //mesa
        dibujaCuadrado(-28, 4, -5, -28, 4, 0, -18, 4, 0, -18, 4, -5, 0.95f, 0.95f, 0.95f);//base mesa
        dibujaCuadrado(-28, 4.3f, -5, -28, 4.3f, 0, -18, 4.3f, 0, -18, 4.3f, -5, 0.95f, 0.95f, 0.95f);//superior
        dibujaCuadrado(-23, 4, -5, -18, 4, -5, -18, 4.3f, -5, -23, 4.3f, -5, 0.85f, 0.85f, 0.85f);//fondo
        dibujaCuadrado(-18, 4, 0, -18, 4, -5, -18, 4.3f, -5, -18, 4.3f, 0, 0.85f, 0.85f, 0.85f);//derecha superior
        dibujaCuadrado(-28, 4, 0, -18, 4, 0, -18, 4.3f, 0, -28, 4.3f, 0, 0.85f, 0.85f, 0.85f);//frente superior
        dibujarCilindroGlut(0.2f, 4.1f, 4, 1, 1, 0.5f, 0, -18.2f, 0, -4.8f, -90, 0,0);//pata fondo
        dibujarCilindroGlut(0.2f, 4.1f, 4, 1, 1, 0.5f, 0, -18.2f, 0, -0.2f, -90, 0,0);//pata frente
        dibujarCilindroGlut(0.2f, 5f, 4, 1, 1, 0.5f, 0, -18.2f, 1.5f, -5f, 0, 0,0);//medio
        dibujarCilindroGlut(0.2f, 4.8f, 4, 1, 1, 0.5f, 0, -18.2f, 3.8f, -4.8f, 0, -90, 0);//soporte fondo
        dibujarCilindroGlut(0.2f, 4.8f, 4, 1, 1, 0.5f, 0, -18.2f, 3.8f, -0.2f, 0, -90, 0);//soporte frente
        
        //lavamanos        
        gl.glPushMatrix();
        gl.glTranslatef(-14, 1.5f, -21.7f);
        gl.glRotatef(90, 1, 0, 0);
        dibujarCirculo(0, 0, 0, 1.5f, 0.9f, 0.9f, 0.9f);//fondo     
        gl.glPopMatrix();
        dibujarCilindro(quad, 1.5f, 1.5f, 1, 20, 20, 0.7f, 0.7f, 0.7f, -14, 1.5f, -21.7f, -90, 0);//lavavo parte inferior
        dibujarCilindroGlut(0.35f, 3, 8, 1, 0.8f, 0.8f, 0.8f, -12.5f, 2.8f, -23, 0, -90,0);//fondo
        dibujarCilindroGlut(0.35f, 3, 8, 1, 0.8f, 0.8f, 0.8f, -12.5f, 2.8f, -20.4f, 0, -90,0);//frente
        dibujarCilindroGlut(0.35f, 3, 8, 1, 0.8f, 0.8f, 0.8f, -12.6f, 2.8f, -23.1f, 0, 0,0);//derecha
        dibujarCilindroGlut(0.35f, 3, 8, 1, 0.8f, 0.8f, 0.8f, -15.5f, 2.8f, -23.1f, 0, 0,0);//izquierda
        dibujaCuadrado(-15.7f, 3, -23, -12.4f, 3, -23f, -12.4f, 4.5f, -23f, -15.7f, 4.5f, -23f, 0.55f, 0.55f, 0.55f);//frente superior
        dibujaCuadrado(-15.7f, 3, -23, -15.7f, 3, -24, -15.7f, 4.5f,-24, -15.7f, 4.5f, -23, 0.7f, 0.7f, 0.7f);//izquierda superior
        dibujaCuadrado(-12.4f, 3, -23, -12.4f, 3, -24, -12.4f, 4.5f,-24, -12.4f, 4.5f, -23, 0.7f, 0.7f, 0.7f);//derecha superior
        dibujaCuadrado(-15.7f, 4.5f, -24, -15.7f, 4.5f, -23, -12.4f, 4.5f, -23, -12.4f, 4.5f, -24, 0.7f, 0.7f, 0.7f);//superior
        dibujarCilindroGlut(0.2f, 2.6f, 8, 1, 0.7f, 0.3f, 0.1f, -12.5f, 0, -20.5f, -90, 0,0);//pata frente derecha
        dibujarCilindroGlut(0.2f, 2.6f, 8, 1, 0.7f, 0.3f, 0.1f, -15.6f, 0, -20.5f, -90, 0,0);//pata frente izquierda
        dibujarCilindroGlut(0.2f, 2.6f, 8, 1, 0.7f, 0.3f, 0.1f, -12.5f, 0, -23f, -90, 0,0);//pata fondo derecha
        dibujarCilindroGlut(0.2f, 2.6f, 8, 1, 0.7f, 0.3f, 0.1f, -15.6f, 0, -23, -90, 0,0);//pata fondo izquierda
        dibujarCilindroGlut(0.2f, 3.2f, 8, 1, 0.7f, 0.3f, 0.1f, -12.5f, 1, -20.5f, 0, -90,0);//soporte frente
        dibujarCilindroGlut(0.2f, 2.6f, 8, 1, 0.7f, 0.3f, 0.1f, -12.5f, 1, -23f, 0, 0,0);//medio derecha
        dibujarCilindroGlut(0.2f, 2.6f, 8, 1, 0.7f, 0.3f, 0.1f, -15.6f, 1, -23f, 0, 0,0);//medio izquierda
        //llaves
        dibujarCirculo(-14, 3.8f, -22.99f, 0.35f, 0, 0, 0);//fondo grifo
        dibujarCilindroGlut(0.15f, 0.7f, 8, 1, 0.95f, 0.95f, 0.95f, -14, 3.8f, -22.99f, 0, 0,0);//grifo sup
        dibujarCilindroGlut(0.15f, 0.4f, 8, 1, 0.95f, 0.95f, 0.95f, -14, 3.7f, -22.45f, 90, 0,0);//grifo frente
        dibujarCilindroGlut(0.1f, 0.3f, 8, 1, 1f, 0.95f, 0.95f, -13.2f, 3.8f, -22.99f, 0, 0,0);//llave derecha
        dibujarCilindroGlut(0.1f, 0.4f, 8, 1, 0.5f, 0.7f, 0.5f, -13.2f, 3.8f, -22.7f, 0, 90,0);//manija derecha
        dibujarCilindroGlut(0.15f, 0.3f, 8, 1, 1f, 0.95f, 0.95f, -14.8f, 3.8f, -22.99f, 0, 0,0);//llave izquierda
        dibujarCilindroGlut(0.1f, 0.4f, 8, 1, 0.5f, 0.7f,0.5f , -14.8f, 3.8f, -22.7f, 0, -90,0);//manija izquierda
        
        //maquinas de lavado
        //maquina izquierda
        dibujarCubo(3, 0.6f, 0.6f, 0.6f, -10, 1.5f, -22.5f,0);//base inferior
        dibujaCuadrado(-11.5f, 3, -23, -8.5f, 3, -23, -8.5f, 4, -24, -11.5f, 4, -24, 0.5f, 0.6f, 0.5f);//base superior
        dibujaTriangulo(-11.5f, 3, -24, -11.5f, 4, -24, -11.5f, 3, -23, 0.5f, 0.5f, 0.5f);//lado izquierdo superior
        dibujaTriangulo(-8.5f, 3, -24, -8.5f, 4, -24, -8.5f, 3, -23, 0.5f, 0.5f, 0.5f);//lado derecho superior
        dibujarCilindroGlut(0.3f, 0.2f, 8, 1, 0.45f, 0.45f, 0.45f, -10, 3.5f, -23.5f, -45, 0,0);//boton medio
        dibujarCubo(0.2f, 0.45f, 0.45f, 0.45f, -8.9f, 3.7f, -23.6f,45);//boton derecho superior
        dibujarCubo(0.2f, 0.45f, 0.45f, 0.45f, -8.9f, 3.4f, -23.3f,45);//boton derecho inferior
        dibujarCubo(0.2f, 0.45f, 0.45f, 0.45f, -9.3f, 3.7f, -23.6f,45);//boton izquierdo superior
        dibujarCubo(0.2f, 0.45f, 0.45f, 0.45f, -9.3f, 3.4f, -23.3f,45);//boton izquierdo inferior
        dibujaCuadrado(-11.3f, 3.01f, -22.8f, -11.3f, 3.01f, -21.2f, -8.7f,3.01f , -21.2f, -8.7f, 3.01f, -22.8f, 0.5f, 0.5f, 0.5f);//tapa superior
        dibujaCuadrado(-11.3f, 0.2f, -20.99f, -10.6f, 0.2f, -20.99f, -10.6f, 0.6f, -20.99f, -11.3f, 0.6f, -20.99f, 0.5f, 0.5f, 0.5f);//adorno inferior
        //maquina derecha
        dibujarCubo(3, 0.5f, 0.6f, 0.5f, -6.5f, 1.5f, -22.5f,0);//base inferior
        dibujaCuadrado(-8, 3, -23, -5, 3, -23, -5, 4, -24, -8, 4, -24, 0.6f, 0.6f, 0.6f);//base superior
        dibujaTriangulo(-8, 3, -24, -8, 4, -24, -8, 3, -23, 0.5f, 0.5f, 0.5f);//lado izquierdo superior
        dibujaTriangulo(-5f, 3, -24, -5f, 4, -24, -5f, 3, -23, 0.5f, 0.5f, 0.5f);//lado derecho superior
        dibujarCilindroGlut(0.3f, 0.2f, 8, 1, 0.45f, 0.45f, 0.45f, -7.6f, 3.5f, -23.5f, -45, 0,0);//boton izquierdo
        
        gl.glPushMatrix();        
            for (int i = 0; i < 6; i++) {                 
                dibujarCubo(0.2f, 0.45f, 0.45f, 0.45f, -7f, 3.55f, -23.6f,45);//botones superiores peque;os
                gl.glTranslatef(0.35f, 0 , 0);  
            }
        gl.glPopMatrix();
        
        dibujaCuadrado(-7.8f, 0.5f, -20.99f, -5.2f, 0.5f, -20.99f, -5.2f, 2.6f, -20.99f, -7.8f, 2.6f, -20.99f, 0.45f, 0.45f, 0.45f);//puerta
        dibujaCuadrado(-5.6f, 1.3f, -20.97f, -5.4f, 1.3f, -20.97f, -5.4f, 2, -20.97f, -5.6f, 2, -20.97f, 0.6f, 0.6f, 0.6f);//manija
        
        //reloj
        dibujarCilindroGlut(1f, 0.2f, 20, 1, 0.3f, 0.3f, 0.3f, -17, 11f, -23.89f, 0, 0,0);//base
        dibujarCirculo(-17, 11,-23.68f, 0.8f, 1, 1, 1);//base manecilla
        
        gl.glPushMatrix();
//        gl.glLineWidth(1.5f); 
        dibujarLinea(-17f, 11, -23.67f, -17f, 11.7f, -23.67f, 0, 0, 0);//manecilla 1
        dibujarLinea(-17f, 11, -23.67f, -16.5f, 11.5f, -23.67f, 0, 0, 0);//manecilla 2
        gl.glPopMatrix();
        
        //cuadro fondo
        dibujarCilindroGlut(3, 0.2f, 4, 1, 0.8f, 0.5f, 0, -25, 8.5f, -23.91f, 0, 0,45);//base1
        dibujarCilindroGlut(3, 0.2f, 4, 1, 0.8f, 0.5f, 0, -20.8f, 8.5f, -23.91f, 0, 0,45);//base2
        dibujaCuadrado(-26.85f, 6.7f, -23.6f, -18.95f, 6.7f, -23.6f, -18.95f, 10.3f, -23.6f, -26.85f, 10.3f, -23.6f, 1f, 0.8f, 0);//fondo
        dibujaCuadrado(-26.3f, 7.8f, -23.59f, -25f, 7.8f, -23.59f, -25f, 9.5f, -23.59f, -26.3f, 9.5f, -23.59f, 1, 0.94f, 0.6f);//papel grande1
        
        gl.glPushMatrix();
        gl.glTranslatef(1.8f, -0.7f, 0);
        dibujaCuadrado(-26.3f, 7.8f, -23.59f, -25f, 7.8f, -23.59f, -25f, 9.5f, -23.59f, -26.3f, 9.5f, -23.59f, 1, 0.94f, 0.6f);//papel grande2
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        gl.glTranslatef(4.7f, -0.5f, 0);
        dibujaCuadrado(-26.3f, 7.8f, -23.59f, -25f, 7.8f, -23.59f, -25f, 9.5f, -23.59f, -26.3f, 9.5f, -23.59f, 1, 0.94f, 0.6f);//papel grande3
        gl.glPopMatrix();
        
        dibujaCuadrado(-26.5f, 9f, -23.58f, -25.5f, 9f, -23.58f, -25.5f, 9.9f, -23.58f, -26.5f, 9.9f, -23.58f, 0.9f, 0.9f, 0.9f);//papel peque;o1
        
        gl.glPushMatrix();
        gl.glTranslatef(2.2f, 0.2f, 0);
        dibujaCuadrado(-26.5f, 9f, -23.58f, -25.5f, 9f, -23.58f, -25.5f, 9.9f, -23.58f, -26.5f, 9.9f, -23.58f, 0.9f, 0.9f, 0.9f);//papel peque;o2
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        gl.glTranslatef(6.2f, 0.2f, 0);
        dibujaCuadrado(-26.5f, 9f, -23.58f, -25.5f, 9f, -23.58f, -25.5f, 9.9f, -23.58f, -26.5f, 9.9f, -23.58f, 1, 0.85f, 0.6f);//papel peque;o3
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        gl.glTranslatef(6f, -1.2f, 0);
        dibujaCuadrado(-26.5f, 9f, -23.58f, -25.5f, 9f, -23.58f, -25.5f, 9.9f, -23.58f, -26.5f, 9.9f, -23.58f, 0.9f, 0.9f, 0.9f);//papel peque;o4
        gl.glPopMatrix();
        
        dibujaCuadrado(-22, 8.8f,-23.57f, -20.5f, 8.8f, -23.57f, -20.5f, 10, -23.57f, -22, 10, -23.57f, 0.95f, 0.95f, 0.95f);//papel mediano
        dibujaCuadrado(-22.3f, 7f,-23.57f, -21.4f, 7f, -23.57f, -21.4f, 8.5f, -23.57f, -22.3f, 8.5f, -23.57f, 0.92f, 0.92f, 0.92f);//papel inferior
        
        //cuadro lateral
        dibujarCilindroGlut(3, 0.2f, 4, 1, 1f, 0.8f, 0, -27.96f, 8.5f, -13.7f, 0, 90,45);//base1
        dibujarCilindroGlut(3, 0.2f, 4, 1, 1f, 0.8f, 0, -27.96f, 8.5f, -18, 0, 90,45);//base2
        dibujarCilindroGlut(1f, 0.2f, 20, 1, 0.3f, 0.3f, 0.3f, -27.74f, 8.5f, -15.75f, 0, 90,45);//rueda central
        dibujarCilindroGlut(0.2f, 0.4f, 20, 1, 0.2f, 0.2f, 0.2f, -27.74f, 7.5f, -12f, 0, 90,45);//rueda pque;a1
        dibujarCilindroGlut(0.2f, 0.4f, 20, 1, 0.2f, 0.2f, 0.2f, -27.74f, 7.8f, -12.9f, 0, 90,45);//rueda peque;a2
        dibujarCilindroGlut(0.2f, 0.4f, 20, 1, 0.2f, 0.2f, 0.2f, -27.74f, 6.8f, -12.9f, 0, 90,45);//rueda pequena3
        dibujarCilindroGlut(0.2f, 0.4f, 20, 1, 0.2f, 0.2f, 0.2f, -27.74f, 9.5f, -19, 0, 90,45);//rueda pequena superior
        dibujarCilindroGlut(0.2f, 8.5f, 4, 1, 0.7f, 0.3f, 0.1f, -27.8f, 10.7f, -20f, 0, 0,45);//base superior
        dibujarCilindroGlut(0.2f, 8.5f, 4, 1, 0.7f, 0.3f, 0.1f, -27.5f, 10.7f, -20f, 0, 0,45);//base superior
        dibujarCilindroGlut(0.2f, 8.5f, 4, 1, 0.7f, 0.3f, 0.1f, -27.2f, 10.7f, -20f, 0, 0,45);//base superior
        
        //anaquel derecho
        dibujarCilindroGlut(0.2f, 6.5f, 4, 1, 0.6f, 0.6f, 0.6f, -5, 6.5f, -13f, 90, 0,0);//pata fondo derecha
        dibujarCilindroGlut(0.2f, 6.5f, 4, 1, 0.6f, 0.6f, 0.6f, -7, 6.5f, -13f, 90, 0,0);//pata fondo izquierda
        dibujarCilindroGlut(0.2f, 6.5f, 4, 1, 0.6f, 0.6f, 0.6f, -5, 6.5f, -6.7f, 90, 0,0);//pata frente derecha
        dibujarCilindroGlut(0.2f, 6.5f, 4, 1, 0.6f, 0.6f, 0.6f, -7, 6.5f, -6.7f, 90, 0,0);//pata frente izquierda

        gl.glPushMatrix();        
            for (int i = 0; i < 3; i++) {                 
                dibujarCilindroGlut(1.5f, 0.2f, 4, 1, 0.7f, 0.7f, 0.7f, -6, 0.5f, -12f, 90, 0,45);//base inferior
                gl.glTranslatef(0, 0 , 2.15f);  
            }
        gl.glPopMatrix();
        
        gl.glPushMatrix();        
            for (int i = 0; i < 3; i++) {                 
                dibujarCilindroGlut(1.5f, 0.2f, 4, 1, 0.7f, 0.7f, 0.7f, -6, 2.5f, -12f, 90, 0,45);//base media 1
                gl.glTranslatef(0, 0 , 2.15f);  
            }
        gl.glPopMatrix();
               
        gl.glPushMatrix();        
            for (int i = 0; i < 3; i++) {                 
                dibujarCilindroGlut(1.5f, 0.2f, 4, 1, 0.7f, 0.7f, 0.7f, -6, 4.5f, -12f, 90, 0,45);//base media 2
                gl.glTranslatef(0, 0 , 2.15f);  
            }
        gl.glPopMatrix();
               
        gl.glPushMatrix();        
            for (int i = 0; i < 3; i++) {                 
                dibujarCilindroGlut(1.5f, 0.2f, 4, 1, 0.7f, 0.7f, 0.7f, -6, 6.5f, -12f, 90, 0,45);//base superior
                gl.glTranslatef(0, 0 , 2.15f);  
            }
        gl.glPopMatrix();
        
        //cajas mesa
        dibujarCilindroGlut(1f, 3, 4, 1, 1, 0.85f, 0.6f, -27.2f, 5f, -24f, 0, 0,45);//caja fondo
        dibujarCilindroGlut(0.8f, 3.2f, 4, 1, 0.3f, 0.3f, 0.3f, -27.2f, 5f, -14f, 0, 0,45);//caja frente
        dibujarCilindroGlut(0.8f, 1f, 4, 1, 0.5f, 0.6f, 0.5f, -25.8f, 5f, -24f, 0, 0,45);//caja derecha
    }
}
